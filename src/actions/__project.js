/* eslint-disable */

import axios from "axios"

const API_URL = 'https://b750-158-140-163-210.ap.ngrok.io'

export async function getProjectPagination({ page, per_page }) {
  const data = {
    page: page,
    per_page: per_page
  }

  const res = await axios.post(`${API_URL}/project`, data).then((response) => {
    console.log(response)
  }).catch((e) => {
    console.log(e)
  })

  return res
}

export async function getProjectDetail({ id }) {
  await axios.get(`${API_URL}/project/${id}`).then((response) => {
    console.log(response)
    return response.json()
  }).catch((e) => {
    console.log(e)
  })
}

export async function getprojectList() {
  const res = await axios.get(`${API_URL}/project/list`)

  return res.json()
}

export function createProject({name}) {
  const data = {
    name: name
  }
  const res = axios.post(`${API_URL}/project/save`, data).then((response) => {
    console.log(response)
  }).catch((e) => {
    console.log(e)
  })
}

export async function updateProject({ id, name }) {
  const data = {
    name: name
  }

  const res = await axios.post(`${API_URL}/project/update/${id}`, data).then((response) => {
    console.log(response)
  }).catch((e) => {
    console.log(e)
  })
}