/* eslint-disable */

import axios from "axios"

const API_URL = 'https://99fe-114-5-104-104.ap.ngrok.io'

export async function getMerchantPagination({ page, per_page }) {
  const data = {
    page: page,
    per_page: per_page
  }

  const res = await axios.post(`${API_URL}/merchants`, data).then((response) => {
    return response.data.response
  }).catch((e) => {
    console.log(e)
  })

  return res
}

export async function getMerchantDetail({ id }) {
  await axios.get(`${API_URL}/merchant/${id}`).then((response) => {
    console.log(response)
    return response.json()
  }).catch((e) => {
    console.log(e)
  })
}

export async function getMerchantList() {
  const res = await axios.get(`${API_URL}/merchant/list`)

  return res.json()
}

export function createMerchant({ name }) {
  const data = {
    name: name
  }
  const res = axios.post(`${API_URL}/merchant/save`, data).then((response) => {
    console.log(response)
  }).catch((e) => {
    console.log(e)
  })
}

export async function updateMerchant({ id, name }) {
  const data = {
    name: name
  }

  const res = await axios.post(`${API_URL}/merchant/update/${id}`, data).then((response) => {
    console.log(response)
  }).catch((e) => {
    console.log(e)
  })
}