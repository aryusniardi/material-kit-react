/* eslint-disable */

import axios from "axios"

const API_URL = 'https://99fe-114-5-104-104.ap.ngrok.io'

export async function getUsersPagination({page, per_page}) {
  const data = {
    page: page,
    per_page: per_page
  }

  const res = await axios.post(`${API_URL}/users`, data).then((response) => {
    return response.data.response
  }).catch((e) => {
    console.log(e)
  })

  return res
}

export async function getUserDetail({id}) {
  await axios.get(`${API_URL}/users/${id}`).then((response) => {
    console.log(response)
    return response.json()
  }).catch((e) => {
    console.log(e)
  })
}

export async function getUserList() {
  const res = await axios.get(`${API_URL}/users/list`)

  return res.data.response
}

export async function updateUser({id, full_name, username, password, email, merchant_id, balance}) {
  const data = {
    full_name: full_name,
    username: username,
    password: password,
    email: email,
    merchant_id: merchant_id,
    balance: balance
  }

  const res = await axios.post(`${API_URL}/users/update/${id}`, data).then((response) => {
    console.log(res)
  }).catch((e) => {
    console.log(e)
  })
}