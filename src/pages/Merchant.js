/* eslint-disable */

import React from 'react'
import { filter } from 'lodash';
import { useEffect, useState } from 'react';
import {Link as RouterLink} from 'react-router-dom'

// material
import CircularProgress from '@mui/material/CircularProgress';
import {
  Button,
  Card,
  Table,
  Stack,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Box,
  TableHead,
  Collapse,
  Typography,
  TableContainer,
  TablePagination,
  IconButton,
  Modal
} from '@mui/material';

// components
import Iconify from 'src/components/Iconify';
import Page from '../components/Page';
import Scrollbar from '../components/Scrollbar';
import SearchNotFound from '../components/SearchNotFound';
import { UserListHead } from '../sections/@dashboard/user';

// import data
import { getMerchantPagination } from 'src/actions/__merchant';
import MerchantListToolbar from 'src/sections/@dashboard/merchants/MerchantListToolbar';
import {KeyboardArrowUp, KeyboardArrowDown} from '@mui/icons-material';
import MerchantMoreMenu from 'src/sections/@dashboard/merchants/MerchantMoreMenu';
import MerchantCreateForm from 'src/sections/@dashboard/merchants/MerchantCreateForm';

// ----------------------------------------------------------------------

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #fff',
  boxShadow: 24,
  p: 4,
};

const TABLE_HEAD = [
  { id: 'id', label: '', alignRight: false },
  { id: 'merchants_name', label: 'Merchants Name', alignRight: false },
  { id: 'project_id', label: 'Project ID', alignRight: false },
  { id: 'usersList', label: 'Total User', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.merchants_name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

const Merchant = () => {
  const [data, setData] = useState()

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState('asc');

  const [orderBy, setOrderBy] = useState('merchants_name');

  const [filterName, setFilterName] = useState('');

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [open, setOpen] = useState(false);

  const [modalCreate, setModalCreate] = useState(false)

  const importData = async () => {
    const data = await getMerchantPagination({page, rowsPerPage})

    setData(data)
  }

  useEffect(() => {
    importData()

    if (!data) {
      <CircularProgress />
    }
  }, [])

  if (!data) {
    return null
  } 

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(data, getComparator(order, orderBy), filterName);

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title="Merchants">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Merchants
          </Typography>
          <Button variant="contained" onClick={() => setModalCreate(!modalCreate)} startIcon={<Iconify icon="eva:plus-fill" />}>
            New Merchant
          </Button>

          <Modal
            open={modalCreate}
            onClose={() => setModalCreate(!modalCreate)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <MerchantCreateForm data={data} />
            </Box>
          </Modal>
        </Stack>

        <Card>
          <MerchantListToolbar filterName={filterName} onFilterName={handleFilterByName} />
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <UserListHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={data.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredUsers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                    const { id, merchants_name, project_id, usersList } = row;

                    return (
                      <React.Fragment>
                        <TableRow
                          hover
                          key={merchants_name}
                          sx={{ '& > *': { borderBottom: 'unset' } }}
                        >
                          <TableCell>
                              <IconButton
                                aria-label="expand row"
                                size="small"
                                onClick={() => setOpen(!open)}
                              >
                                {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                              </IconButton>
                            </TableCell> 
                          <TableCell component="th" scope="row" padding="normal">
                            <Stack direction="row" alignItems="center" spacing={2}>
                              <Typography variant="subtitle2" noWrap>
                                {merchants_name}
                              </Typography>
                            </Stack>
                          </TableCell>
                          <TableCell align="left">{project_id ? project_id : ' - '}</TableCell>
                          <TableCell align="left">{usersList ? usersList.length : ' 0 '}</TableCell>
                          <TableCell align="right">
                            <MerchantMoreMenu
                              id={id}
                              merchantsName={merchants_name}
                              projectId={project_id}
                              users={usersList}
                            />
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                            <Collapse in={open} timeout="auto" unmountOnExit>
                              <Box sx={{ margin: 1 }}>
                                <Table size="small" aria-label="user-list">
                                  <TableHead>
                                    <TableRow>
                                      <TableCell>Username</TableCell>
                                      <TableCell>Email</TableCell>
                                      <TableCell>Full Name</TableCell>
                                      <TableCell>Merchant ID</TableCell>
                                      <TableCell align="right">Balance</TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    {usersList.map((item) => (
                                      <TableRow key={item.merchant_name}>
                                        <TableCell>{item.username}</TableCell>
                                        <TableCell>{item.email}</TableCell>
                                        <TableCell>{item.full_name}</TableCell>
                                        <TableCell>{item.merchant_id}</TableCell>
                                        <TableCell align="right">{parseInt(item.balance)}</TableCell>
                                      </TableRow>
                                    ))}
                                  </TableBody>
                                </Table>
                              </Box>
                            </Collapse>
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>

                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}

export default Merchant