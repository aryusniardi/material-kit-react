/* eslint-disable */

import * as Yup from 'yup';
import { useFormik, Form, FormikProvider } from 'formik';
import { useNavigate } from 'react-router-dom';
// material
import { Stack, TextField, Alert, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// component
import axios from 'axios';

// ----------------------------------------------------------------------

export default function MerchantUpdateForm({ data }) {
  const navigate = useNavigate();

  const UpdateSchema = Yup.object().shape({
    name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Merchant name is required'),
    project_id: Yup.number(),
  });

  const formik = useFormik({
    initialValues: {
      name: data.merchant_name,
      project_id: data.project_id,
    },
    validationSchema: UpdateSchema,
    onSubmit: (values) => {
      const url = `https://99fe-114-5-104-104.ap.ngrok.io/merchants/update/${data.id}`

      axios.post(url, values, {
        headers: {
          'Authorization': `${localStorage.getItem('user')}`
        }
      }).then((response) => {
        console.log(response)
        if (response.data.errors) {
          console.log(response.data.errors);
          alert(response.data.errors.merchant_name)
          setSubmitting(false)
        } else {
          location.reload()
        }
      }).catch((e) => {
        console.log(e)
      })
    },
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Typography variant="h6" gutterBottom>
            Edit merchant with ID {data.id}
          </Typography>

          <TextField
            fullWidth
            type="text"
            label="Merchant Name"
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />

          <TextField
            fullWidth
            type="number"
            label="Project ID"
            {...getFieldProps('project_id')}
            error={Boolean(touched.project_id && errors.project_id)}
            helperText={touched.project_id && errors.project_id}
          />

          <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting}>
            Update
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
