/* eslint-disable */

import * as Yup from 'yup';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// component
import axios from 'axios';

// ----------------------------------------------------------------------

export default function MerchantCreateForm({ data }) {
  const UpdateSchema = Yup.object().shape({
    name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Merchant name is required'),
    project_id: Yup.number(),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      project_id: '',
    },
    validationSchema: UpdateSchema,
    onSubmit: (values) => {
      const url = `https://99fe-114-5-104-104.ap.ngrok.io/merchants/save`

      axios.post(url, values, {
        headers: {
          'Authorization': `${localStorage.getItem('user')}`
        }
      }).then((response) => {
        console.log(response)
        if (response.data.errors) {
          console.log(response.data.errors);
          alert(response.data.errors.merchant_name)
          formik.setSubmitting(false)
        } else {
          location.reload()
        }
      }).catch((e) => {
        console.log(e)
      })
    },
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Typography variant="h6" gutterBottom>
            Create new merchant
          </Typography>

          <TextField
            fullWidth
            type="text"
            label="Merchant Name"
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />

          <TextField
            fullWidth
            type="number"
            label="Project ID"
            {...getFieldProps('project_id')}
            error={Boolean(touched.project_id && errors.project_id)}
            helperText={touched.project_id && errors.project_id}
          />

          <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting}>
            Create
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
