/* eslint-disable */

import * as Yup from 'yup';
import { useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
import { useNavigate } from 'react-router-dom';
// material
import { Stack, TextField, IconButton, InputAdornment, Alert, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// component
import Iconify from '../../../components/Iconify';
import axios from 'axios';

// ----------------------------------------------------------------------

export default function UserUpdateForm({data}) {
  const navigate = useNavigate();

  const [showPassword, setShowPassword] = useState(false);

  const UpdateSchema = Yup.object().shape({
    full_name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Full name is required'),
    username: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('First name required'),
    email: Yup.string().email('Email must be a valid email address').required('Email is required'),
    password: Yup.string().min(2, 'Too Short!').max(25, 'Too Long!').required('Password is required'),
    merchant_id: Yup.number(),
    balance: Yup.number()
  });

  const formik = useFormik({
    initialValues: {
      full_name: data.full_name,
      username: data.username,
      email: data.email,
      password: data.password,
      merchant_id: data.merchant_id,
      balance: data.balance,
    },
    validationSchema: UpdateSchema,
    onSubmit: (values) => {
      const url = `https://99fe-114-5-104-104.ap.ngrok.io/users/update/${data.id}`

      xios.post(url, values, {
        headers: {
          'Authorization': `${localStorage.getItem('user')}`
        }
      }).then((response) => {
        console.log(response)
        if (response.data.errors) {
          console.log(response.data.errors);
          alert(response.data.errors.username ? response.data.errors.username : response.data.errors.email)
          setSubmitting(false)
        } else {
          <Alert severity="success">{response.data.message}</Alert>
          navigate('/dashboard/user', { replace: true })
        }
      }).catch((e) => {
        console.log(e)
      })
    },
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Typography variant="h6" gutterBottom>
            Edit user with ID {data.id}
          </Typography>

          <TextField
            fullWidth
            autoComplete="fullName"
            type="text"
            label="Full Name"
            {...getFieldProps('full_name')}
            error={Boolean(touched.full_name && errors.full_name)}
            helperText={touched.full_name && errors.full_name}
          />

          <TextField
            fullWidth
            autoComplete="username"
            type="text"
            label="Username"
            {...getFieldProps('username')}
            error={Boolean(touched.username && errors.username)}
            helperText={touched.username && errors.username}
          />

          <TextField
            fullWidth
            autoComplete="email"
            type="email"
            label="Email address"
            {...getFieldProps('email')}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />

          <TextField
            fullWidth
            autoComplete="current-password"
            type={showPassword ? 'text' : 'password'}
            label="Password"
            {...getFieldProps('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton edge="end" onClick={() => setShowPassword((prev) => !prev)}>
                    <Iconify icon={showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'} />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
          />

          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              label="Merchant ID"
              {...getFieldProps('merchant_id')}
              error={Boolean(touched.merchant_id && errors.merchant_id)}
              helperText={touched.merchant_id && errors.merchant_id}
            />

            <TextField
              fullWidth
              label="Balance"
              {...getFieldProps('balance')}
              error={Boolean(touched.balance && errors.balance)}
              helperText={touched.balance && errors.balance}
            />
          </Stack>

          <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting}>
            Update
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
